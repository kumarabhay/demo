FROM python:3.6.9

COPY . /app
WORKDIR /app
RUN pip install flask requests pytest
ENTRYPOINT ["python"]
CMD ["hello_world.py"]