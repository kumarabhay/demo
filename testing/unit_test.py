import json
import pytest
import requests
import logging
from hello_world import hello_world
'''
@pytest.fixture
def client(request):
    test_client = hello_world.hello()

    def teardown():
        pass # databases and resourses have to be freed at the end. But so far we don't have anything

    request.addfinalizer(teardown)
    return test_client
'''
def post_json(client, url, json_dict):
    """Send dictionary json_dict as a json to the specified url """
    return client.post(url, data=json.dumps(json_dict), content_type='application/json')

def json_of_response(response):
    """Decode json from response"""
    return json.loads(response.data.decode('utf8'))


def test_dummy():
    # setup mock for service to service comm.
    #capsys.get('http://localhost:5001/api/helloapi2/test', text='hii')
    #assert 'hii' == requests.get('http://localhost:5001/api/helloapi2/test').text


    assert "Hello from Python flask docker image..." == hello_world.hello()

'''
    response = client.get('/api/helloappservice/hello')
    assert b'Hello from Python flask docker image...' in response.data
    
'''

