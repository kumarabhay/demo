import logging, os, requests
import flask
from flask import Flask, request, Response, json


logging.basicConfig(level=logging.DEBUG)

hello_world = Flask(__name__)
hello_world.config.from_json("config.json")


author = os.environ.get('AUTHOR_ENV',  hello_world.config.get("AUTHOR"))
loglevel = os.environ.get('LOGLEVEL_ENV',  hello_world.config.get("LOGLEVEL"))
# check if HELLO-API2-URL is specified in the environment variable - if so use it (this will be used by kubernetes)
# for development use the config object directly.
helloapi2url = os.environ.get('HELLOAPI2URL_ENV',  hello_world.config['HELLO-API2-URL'])


@hello_world.route("/")
def hello():
    logging.info("Check config value for: author=%s, log level = %s", author, loglevel)

    # test service to another service communication.
    logging.info("Invoke uri = %s/api/helloapi2/test", helloapi2url)
    resp = requests.get(helloapi2url + '/api/helloapi2/test')
    if resp.status_code == requests.codes.get('ok'):
        response = resp.text
    else:
        response = "error"

    #print(response)
    # end service to service comm.

    return "Hello from Python flask docker image..."


if __name__ == "__main__":
    hello_world.run(debug=True,port='5000')


